import React, { Component } from 'react';
import './App.css';
import SignIn from './SignIn/SignIn';
import 'bootstrap/dist/css/bootstrap.min.css';
import SignUp from './SignUp/SignUp';
import { NavLink, Switch, Route } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Home from './Home/Home';
import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Cookies from 'js-cookie';
import AuthService from './services/auth.service';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Login from './components/login.component';
import Register from './components/register.component';
import Home2 from './components/home.component';
import BoardUser from './components/board-user.component';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import BoardModerator from './components/board-moderator.component';
import BoardAdmin from './components/board-admin.component';
import ButtonAppBar from './NavBar/NavBar';
import ButtonAppBarLogout from './NavBarLogout/NavBarLogout';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import styles from './Home/Home.module.css';
import grey from '@material-ui/core/colors';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showModeratorBoard : false,
			showAdminBoard     : false,
			currentUser        : undefined
		};
	}
	componentDidMount() {
		const user = AuthService.getCurrentUser();

		if (user) {
			this.setState({
				currentUser        : AuthService.getCurrentUser(),
				showModeratorBoard : user.roles.includes('ROLE_MODERATOR'),
				showAdminBoard     : user.roles.includes('ROLE_ADMIN')
			});
		}
	}
	logOut() {
		AuthService.logout();
	}
	render() {
		const { currentUser, showModeratorBoard, showAdminBoard } = this.state;

		const darkTheme = createMuiTheme({
			palette : {
				main : {
					secondary : '#424242'
				},
				type : 'dark'
			}
		});
		const user = AuthService.getCurrentUser;

		return (
			<div className="app">
				<ThemeProvider theme={darkTheme}>
					<Router>
						<div>
							<nav className="navbar navbar-expand navbar-dark bg-dark" style={{ padding: '.1em 1rem' }}>
								<div className="navbar-nav mr-auto">
									<li className="nav-item">
										<a href="/" className="nav-link">
											Strona Główna
										</a>
									</li>

									{showModeratorBoard && (
										<li className="nav-item">
											<NavLink to={'/mod'} className="nav-link">
												Moderator Board
											</NavLink>
										</li>
									)}

									{showAdminBoard && (
										<li className="nav-item">
											<a href="/admin" className="nav-link">
												Panel Administratora
											</a>
										</li>
									)}
								</div>

								{currentUser ? (
									<div className="navbar-nav ml-auto">
										<li className="nav-item">
											<Button
												href="/Login"
												className={styles.loginButton}
												style={{ margin: 10 }}
												id="loginButton"
												variant="outlined"
												onClick={this.logOut}
											>
												Wyloguj
											</Button>
										</li>
									</div>
								) : (
									<div className="navbar-nav ml-auto">
										<li className="nav-item">
											<Button
												href="/Login"
												className={styles.loginButton}
												style={{ margin: 10 }}
												id="loginButton"
												variant="contained"
											>
												Logowanie
											</Button>
										</li>

										<li className="nav-item">
											<Button
												href="/Register"
												className={styles.loginButton}
												style={{ margin: 10 }}
												id="loginButton"
												variant="contained"
											>
												Rejestracja
											</Button>
										</li>
									</div>
								)}
							</nav>
						</div>
					</Router>
					<Main />
				</ThemeProvider>
			</div>
		);
	}
}
export default App;

const Main = () => (
	<Switch>
		<Route exact path="/" component={Home} />
		<Route exact path="/Register" component={Register} />
		<Route exact path="/Login" component={Login} />
		<Route exact path="/SignIn" component={SignIn} />
		<Route exact path="/admin" component={BoardAdmin} />
	</Switch>
);
