import React, { Component } from 'react';
import AuthService from '../services/auth.service';

export default class Profile extends Component {
	constructor(props) {
		super(props);

		this.state = {
			currentUser : AuthService.getCurrentUser()
		};
	}

	render() {
		const { currentUser } = this.state;

		return (
			<div className="container" >
				<br></br>
				<h3>
				<p style={{ color: 'black' }}>
					<strong>Witaj </strong> {currentUser.username}!
				</p>
				</h3>
				<p style={{ color: 'black' }}>
					<strong>Email:</strong> {currentUser.email}
				</p>
				<p style={{ color: 'black' }}>
					<strong>Role w serwisie:</strong>
					<ul>{currentUser.roles && currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}</ul>
				</p>
			</div>
		);
	}
}
