import axios from 'axios';
import authHeader from './auth-header';

const API_URL_BOOK = 'http://library-env.eba-hbvdk2fa.us-east-1.elasticbeanstalk.com/api/book/';
const API_URL_AUTHOR = 'http://library-env.eba-hbvdk2fa.us-east-1.elasticbeanstalk.com/api/author/';
const API_URL_USER = 'http://library-env.eba-hbvdk2fa.us-east-1.elasticbeanstalk.com/api/user/';

class UserService {
	deleteBook(id) {
		return axios.delete(API_URL_BOOK + 'delete/' + id, { headers: authHeader() });
	}
	putBorrowBook(id) {
		return axios.put(API_URL_BOOK + 'borrow/' + id, '', { headers: authHeader() });
	}
	putReturnBook(id) {
		return axios.put(API_URL_BOOK + 'return/' + id, '', { headers: authHeader() });
	}
	getPublicContent() {
		return axios.get(API_URL_BOOK + 'get/all', { headers: authHeader() });
	}
	getAllBooks() {
		const user = JSON.parse(localStorage.getItem('user'));
		var request = new XMLHttpRequest();
		request.open('GET', API_URL_BOOK + 'get/all', false);
		request.setRequestHeader('Authorization', 'Bearer ' + user.accessToken);
		request.send(null);
		if (request.status === 200) {
			console.log(JSON.parse(request.response));
			return JSON.parse(request.response);
		}
		else {
			console.log('xmlhttprequest nie zadzialalo :??0');
			return [];
		}
	}
	getBookById(id) {
		return axios.get(API_URL_BOOK + 'get/' + id, { headers: authHeader() });
	}
	addNewBook(book) {
		return axios.post(API_URL_BOOK + 'add', book, {
			headers : authHeader()
		});
	}
	addNewAuthor(author) {
		return axios.post(API_URL_AUTHOR + 'add', author, {
			headers : authHeader()
		});
	}
	getAllUsers() {
		return axios.get(API_URL_USER + 'get/all', { headers: authHeader() });
	}
	deleteUser(id) {
		return axios.delete(API_URL_USER + 'delete/' + id, { headers: authHeader() });
	}
	deleteAuthor(id) {
		return axios.delete(API_URL_AUTHOR + 'delete/' + id, { headers: authHeader() });
	}
	getAllAuthors() {
		const user = JSON.parse(localStorage.getItem('user'));
		var request = new XMLHttpRequest();
		request.open('GET', API_URL_AUTHOR + 'get/all', false);
		request.setRequestHeader('Authorization', 'Bearer ' + user.accessToken);
		request.send(null);
		if (request.status === 200) {
			console.log(JSON.parse(request.response));
			return JSON.parse(request.response);
		}
		else {
			console.log('xmlhttprequest nie zadzialalo :??0');
			return [];
		}
	}
	getUserBoard() {
		return axios.get('http://library-env.eba-hbvdk2fa.us-east-1.elasticbeanstalk.com/api/user/get/all', { headers: authHeader() });
	}

	getModeratorBoard() {
		return axios.get(API_URL_BOOK + 'mod', { headers: authHeader() });
	}

	getAdminBoard() {
		return axios.get(API_URL_BOOK + 'admin', { headers: authHeader() });
	}
}

export default new UserService();
